package edu.bu.ec504.hw3.bst;

public class ExtendedBSTSet<keyType extends Comparable<keyType>> extends BSTSet<keyType> {

  // METHODS

  /**
   * @return The parent of the current node.
   */
  public ExtendedBSTSet<keyType> getParent() {
    return parent;
  }

  /**
   * Perform the rotation specified in <code>op</code> on the current tree.
   * @param op A rotational operation to complete.
   * @return true iff the rotation was successful.
   */
  public boolean rotate(BSTRotation<keyType> op) {
    return false;
  }

  /**
   * @inheritDoc
   */
  @Override
  protected searchRecord searchHelper(keyType searchKey) {
    return super.searchHelper(searchKey);
  }

  /**
   * @inheritDoc
   * @return
   */
  @Override
  public boolean add(keyType newKey) {
    return super.add(newKey);
  }

  /**
   * @inheritDoc
   */
  @Override
  public String toString() {
    return super.toString();
  }

  /**
   * @inheritDoc
   */
  @Override
  String toString(String prefix) {
    return super.toString(prefix);
  }

  // FIELDS
  protected ExtendedBSTSet<keyType> parent;  // the parent of this node, or null if this node has no parent (e.g. it is the global root)
}
