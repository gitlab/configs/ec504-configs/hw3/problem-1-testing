package edu.bu.ec504.hw3.Tests.TestExtendedBST;

import edu.bu.ec504.hw3.bst.ExtendedBSTSet;

import java.util.Arrays;

public class TestParentPointer {

  /**
   * @param node The root of thet ree to search.
   * @return The number of null parents in the nodes of the subtree rooted at <code>node</code>.
   */
  private static int numNullParents(ExtendedBSTSet node) {
    if (node == null)
      return 0;

    return (node.getParent() == null ? 1 : 0) // me
            + numNullParents((ExtendedBSTSet) node.getLeftChild()) // left child
            + numNullParents((ExtendedBSTSet) node.getRightChild()); // right child
  }
  public static void main(String[] args) {
    ExtendedBSTSet<Integer> intSet = new ExtendedBSTSet<>();
    Integer ints[] = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    intSet.addAll(Arrays.asList(ints));
    System.out.println("The tree:\n"+intSet);

    // check root node
    System.out.print("Checking root node");
    if (intSet.getKey()!=0 || intSet.getParent()!=null)
      throw new RuntimeException("Root node is misformed.");
    System.out.println("... ok");

    // check that all but one element has a parent
    System.out.print("Checking the number of null parents in the tree");
    if (numNullParents(intSet)!=1)
      throw new RuntimeException("Too many null parents found: "+numNullParents(intSet)+" vs. 1");
    System.out.println("... ok");
  }


}
